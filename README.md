# Материалы дисциплины Современные технологии разработки ПО (магистры)
## Тестовый (примерный) проект курсового проекта.


### Порядок работы с примером

0. git clone https://gitlab.com/volodink/jupyter-course

### Для работы с блокнотами:

1. docker-compose build

2. docker-compose up

2.1 Откройте ссылку, представленную в журнале работы в терминальном окне.

Для окончания работы нажмите в терминале:
```
CTRL + C
```

3. docker-compose down


### Для старта сервера с web-интерфейсом:

1. docker-compose -f panel-docker-compose.yml build --no-cache

2. docker-compose -f panel-docker-compose.yml up

2.1 Откройте ссылку, представленную в журнале работы в терминальном окне.

...

Для окончания работы нажмите в терминале:
```
CTRL+C
```

3. docker-compose -f panel-docker-compose.yml down

### Структура КП

1. ТЛ
2. Задание
3. Содержание
4. Введение
5. Анализ предметной области
6. Модели
7. Реализация
8. Заключение
9. Библиографический список
10. Приложения


### Запуск в binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/volodink%2Fjupyter-course/master)
