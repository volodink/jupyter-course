FROM python:3.7

COPY requirements.txt /app/

RUN pip install -r /app/requirements.txt

CMD ["rm", "-rf", "app"]

CMD ["jupyter", "nbextension", "enable", "--py", "widgetsnbextension"]

CMD ["jupyter",  "labextension", "install", "@pyviz/jupyterlab_pyviz"]

CMD ["jupyter-nbextension", "install", "rise", "--py", "--sys-prefix"]

# Run jupyter notebook
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--notebook-dir=/home/app", "--allow-root"]
